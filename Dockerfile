FROM python:3.9

RUN mkdir -p /opt/services/wl-backend

WORKDIR /opt/services/wl-backend

ADD . /opt/services/wl-backend/

RUN chmod 755 /opt/services/wl-backend/scripts/* && \
        chmod +x /opt/services/wl-backend/scripts/* && \
            export DJANGO_SETTINGS_MODULE=wl.settings && \
                pip install -r requirements.txt 