from django.contrib import admin

from chat.models import Room, Message


class UserRoomInline(admin.TabularInline):
    model = Room.current_users.through
    extra = 0


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ("id","name", "host") 
    list_display_links = ("name",)
    fields = (
        "name",
        "host",
    )
    readonly_fields = (
        "created_at",
        "updated_at",
    )
    search_fields = ["title", "symbol"]
    ordering = ["-created_at"]
    list_per_page = 25
    inlines = [UserRoomInline]


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ("id","room", "text") 
    list_display_links = ("text",)
    fields = (
        "room",
        "text",
        "user",
        "created_at"
    )
    readonly_fields = (
        "created_at",
        "updated_at",
    )
    search_fields = ["text", "room", "user"]
    ordering = ["-created_at"]
    list_per_page = 25