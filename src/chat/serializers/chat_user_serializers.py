from rest_framework import serializers

from user.models import User


class UserChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        field = ["id", "username", "email", "password"]
        exclude = ["password"]