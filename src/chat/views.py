from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, reverse

from chat.models import Room


def index(request):
    if request.method == "POST":
        name = request.POST.get("name", None)
        if name:
            room = Room.objects.create(name=name, host=request.user)
            HttpResponseRedirect(reverse("room", args=[room.pk]))
            print(room.pk)
    return render(request, 'chat/index.html')

def room(request, pk):
    room: Room = get_object_or_404(Room, pk=pk)
    return render(request, 'chat/room.html', {
        "room":room,
    })

def test(request):
    return render(request, 'chat/test.html')

def get_crypto_currencies(request):
    return render(request, "crypto/index.html")

def get_user_rooms(request):
    return render(request, "chat/chat_list.html")