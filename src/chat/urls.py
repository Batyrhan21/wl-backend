from django.urls import path

from . import views

urlpatterns = [
    path("crypto/list", views.get_crypto_currencies, name="list-crypto"),
    path("room/list", views.get_user_rooms, name="list-user-rooms"),
    path("room/<int:pk>/", views.room, name="room"),
    path("", views.index, name="index"),
]
