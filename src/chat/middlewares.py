import os
import jwt
from datetime import datetime

import django
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from channels.db import database_sync_to_async
from channels.middleware import BaseMiddleware

from user.models import User
from django.db import close_old_connections


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
django.setup()

ALGORITHM = "HS256"


@database_sync_to_async
def get_user(token):
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=ALGORITHM)
    except:
        return AnonymousUser()

    token_exp = datetime.fromtimestamp(payload['exp'])
    if token_exp < datetime.utcnow():
        return AnonymousUser()

    try:
        user = User.objects.get(id=payload['user_id'])
    except User.DoesNotExist:
        return AnonymousUser()

    return user


class TokenAuthMiddleware(BaseMiddleware):

    async def __call__(self, scope, receive, send):
        close_old_connections()
        
        headers = dict(scope.get("headers", []))
        token_key = headers.get(b"authorization", b"").decode("utf-8").split(" ")[-1]

        scope['user'] = await get_user(token_key)
        
        return await super().__call__(scope, receive, send)
    

def JwtAuthMiddlewareStack(inner):
    return TokenAuthMiddleware(inner)