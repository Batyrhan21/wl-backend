from djangochannelsrestframework import permissions
from djangochannelsrestframework.generics import GenericAsyncAPIConsumer
from djangochannelsrestframework.mixins import ListModelMixin
from djangochannelsrestframework.observer import model_observer

from cryptocurrencies.models import Cryptocurrency
from cryptocurrencies.serializers.crypto_serializers import CryptoCurrenciesSerializer


class CryptoCurrenciesConsumer(ListModelMixin, GenericAsyncAPIConsumer):

    queryset = Cryptocurrency.objects.all()
    serializer_class = CryptoCurrenciesSerializer 
    permissions = (permissions.AllowAny,)

    async def connect(self, **kwargs):
        await self.model_change.subscribe()
        await super().connect()

    @model_observer(Cryptocurrency)
    async def model_change(self, message, observer=None, **kwargs):
        await self.send_json(message)

    @model_change.serializer
    def model_serialize(self, instance, action, **kwargs):
        return dict(data=CryptoCurrenciesSerializer(instance=instance).data, action=action.value)