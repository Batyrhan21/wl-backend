from django.contrib import admin


from cryptocurrencies.models import Cryptocurrency

@admin.register(Cryptocurrency)
class CryptoAdmin(admin.ModelAdmin):
    list_display = ("title", "symbol", "price")
    list_display_links = ("title",)
    fields = (
        "title",
        "symbol",
        "price",
    )
    readonly_fields = (
        "created_at",
        "updated_at",
    )
    search_fields = ["title", "symbol"]
    ordering = ["-created_at"]
    list_per_page = 25