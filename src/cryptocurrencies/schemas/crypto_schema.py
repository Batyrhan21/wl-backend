import coreapi
from rest_framework.schemas.coreapi import AutoSchema
import coreschema


class CartItemSchema(AutoSchema):
    def get_manual_fields(self, path, method):
        api_fields = []
        if method == "GET":
            api_fields = [
                coreapi.Field(
                    name="status",
                    required=True,
                    location="query",
                    schema=coreschema.String(description="status"),
                )
            ]
        if method == "POST":
            api_fields = [
                coreapi.Field(
                    name="product_id",
                    required=True,
                    location="form",
                    schema=coreschema.String(description="Product object's ID"),
                )
            ]
        if method == "DELETE":
            api_fields = [
                coreapi.Field(
                    name="product_id",
                    required=True,
                    location="path",
                    schema=coreschema.String(description="Product object's ID"),
                )
            ]
        if method == "PATCH":
            api_fields = [
                coreapi.Field(
                    name="product_id",
                    required=True,
                    location="path",
                    schema=coreschema.String(description="Product object's ID"),
                ),
                coreapi.Field(
                    name="quantity",
                    required=True,
                    location="query",
                    example="increment, decrement",
                    schema=coreschema.String(description="'increment' or 'decrement'"),
                ),
            ]
        return self._manual_fields + api_fields


class CleanCartSchema(AutoSchema):
    def get_manual_fields(self, path, method):
        api_fields = []
        if method == "POST":
            api_fields = [
                coreapi.Field(
                    name="cleanMethod",
                    required=True,
                    location="query",
                    schema=coreschema.String(
                        description="cart clean method: 'all', 'inactive'"
                    ),
                )
            ]

        return self._manual_fields + api_fields
