from django.db import models

from common.models import BaseModel

class Cryptocurrency(BaseModel):
    title = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name of cryptocurrency")
    symbol = models.SlugField(max_length=255, null=True, unique=True, db_index=True, verbose_name=("Symbol"))
    price = models.DecimalField(max_digits=20, decimal_places=10, null=True, blank=True, verbose_name="Price")

    def __str__(self) -> str:
        return self.title

    class Meta:
        verbose_name = "Cryptocurrency"
        verbose_name_plural = "Cryptocurrencies"