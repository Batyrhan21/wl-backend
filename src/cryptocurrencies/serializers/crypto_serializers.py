from rest_framework import serializers


class CryptoCurrenciesSerializer(serializers.Serializer):
    id = serializers.UUIDField(read_only=True)
    title = serializers.CharField(read_only=True)
    symbol = serializers.CharField(read_only=True)
    price = serializers.DecimalField(read_only=True, max_digits=20, decimal_places=10)