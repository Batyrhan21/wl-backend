import websockets

from django.conf import settings

from channels.generic.websocket import AsyncWebsocketConsumer

class CryptoListConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        await self.accept()
        await self.subscribe_to_binance()

    async def disconnect(self, close_code):
        pass

    async def receive(self, text_data):
        pass

    async def subscribe_to_binance(self):
        url = settings.BINANCE_WS_API_URL
        async with websockets.connect(url) as websocket:
            while True:
                message = await websocket.recv()
                if self.channel_name:
                    await self.send(message)
                else:
                    break