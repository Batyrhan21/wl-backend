from django.urls import re_path

from chat.consumers.crypto_consumers import CryptoCurrenciesConsumer
from chat.consumers.room_consumers import RoomConsumer
from cryptocurrencies.consumers.binance_consumer import CryptoListConsumer


websocket_urlpatterns = [
    re_path(r"^ws/crypto/$", CryptoCurrenciesConsumer.as_asgi()),
    re_path(r"^ws/cryptocurrencies/$", CryptoListConsumer.as_asgi()),
    re_path(r"^ws/room/$", RoomConsumer.as_asgi()),

]
