import requests
from typing import Union

from django.urls import reverse
from django.conf import settings
from django.http import HttpRequest
from django.utils.deprecation import MiddlewareMixin
from rest_framework.response import Response
from user.services.auth_service import CookieService, TokenService

from user.services.oauth_service import GoogleOAuthService


class GoogleCallbackRedirect(MiddlewareMixin):
    def process_response(
        self, request: HttpRequest, response: Response
    ) -> Union[None, Response]:
        if request.build_absolute_uri().split("?")[0] == request.build_absolute_uri(
            reverse("google-login-callback")
        ):
            user = GoogleOAuthService.google_login_callback(
                error=request.GET.get("error"),
                code=request.GET.get("code"),
                callback_url=request.build_absolute_uri(
                    reverse("google-login-callback")
                ),
            )
            token = TokenService.create_auth_token_by_user(user)
            CookieService.set_auth_cookie(response, token, token_type="access")
            CookieService.set_auth_cookie(response, token, token_type="refresh")
            response.data = {
                "code": settings.STATUS_CODES.get("user_signed_in"),
            }
            return response
        return response
