import requests
from typing import Union

from django.urls import reverse
from django.conf import settings
from django.http import HttpRequest
from django.utils.deprecation import MiddlewareMixin

from typing import Any
from jose import jwt as jose_jwt

from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.authentication import JWTAuthentication


class TokenRefreshMiddleware(MiddlewareMixin):
    def process_request(self, request: HttpRequest) -> Union[None, Response]:
        try:
            jwt_auth = JWTAuthentication()
            auth_header = jwt_auth.get_header(request)
            if auth_header is None:
                access_token, old_refresh_token = (
                    request.COOKIES.get(settings.SIMPLE_JWT["AUTH_COOKIE"]) or None,
                    request.COOKIES.get(settings.SIMPLE_JWT["REFRESH_COOKIE"]) or None,
                )
            else:
                access_token: Any = jwt_auth.get_raw_token(auth_header)
                old_refresh_token =  None

            if access_token is None or old_refresh_token is None:
                return

            response = self.get_response(request)
            try:
                jose_jwt.decode(
                    access_token,
                    settings.SIMPLE_JWT["SIGNING_KEY"],
                    algorithms=[settings.SIMPLE_JWT["ALGORITHM"]],
                )
            except:
                refreshed_token: str = self.refresh_token(
                    old_refresh_token,
                    enpoint=request.build_absolute_uri(reverse("token-refresh")),
                )
                response.set_cookie(
                    key=settings.SIMPLE_JWT["AUTH_COOKIE"],
                    value=refreshed_token,
                    expires=settings.SIMPLE_JWT["ACCESS_TOKEN_LIFETIME"],
                    secure=settings.SIMPLE_JWT["AUTH_COOKIE_SECURE"],
                    httponly=settings.SIMPLE_JWT["AUTH_COOKIE_HTTP_ONLY"],
                    samesite=settings.SIMPLE_JWT["AUTH_COOKIE_SAMESITE"],
                )
            try:
                jose_jwt.decode(
                    old_refresh_token,
                    settings.SIMPLE_JWT["SIGNING_KEY"],
                    algorithms=[settings.SIMPLE_JWT["ALGORITHM"]],
                )
            except:
                response.delete_cookie(settings.SIMPLE_JWT["AUTH_COOKIE"])
                response.delete_cookie(settings.SIMPLE_JWT["REFRESH_COOKIE"])
            return response

        except TokenError:
            pass

    @staticmethod
    def refresh_token(old_token, enpoint) -> str:
        resp: requests.Response = requests.post(
            url=enpoint, json={"refresh": old_token}
        )
        access_token: str = resp.json().get("access")
        return access_token

    @staticmethod
    def decode_token(token) -> str:
        try:
            decoded_token = jose_jwt.decode(
                token,
                settings.SIMPLE_JWT["SIGNING_KEY"],
                algorithms=[settings.SIMPLE_JWT["ALGORITHM"]],
            )
            return decoded_token
        except:
            pass
