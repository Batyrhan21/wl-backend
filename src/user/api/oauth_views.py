from django.urls import reverse
from django.http import HttpRequest, HttpResponseRedirect
from django.conf import settings

from rest_framework import views
from rest_framework.permissions import AllowAny

from user.services.oauth_service import GoogleOAuthService


class GoogleLoginAPIView(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request: HttpRequest, *args, **kwargs):
        response = GoogleOAuthService.google_login(
            self.request.build_absolute_uri(reverse("google-login-callback"))
        )
        return HttpResponseRedirect(response)


class GoogleLoginCallbackAPIView(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request: HttpRequest, *args, **kwargs):
        return HttpResponseRedirect(
            f"{settings.CLIENT_BASE_URL}/auth/google/login/callback/?status=success"
        )
