import threading
from threading import Thread
from django.conf import settings
from django.core.mail import EmailMessage

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags


class EmailThread(threading.Thread):
    def __init__(self, subject, html_content, recipient_list):
        self.subject = subject
        self.recipient_list = recipient_list
        self.html_content = html_content
        threading.Thread.__init__(self)

    def run(self) -> None:
        """_send email to user in different thread_"""
        msg = EmailMessage(
            subject=self.subject,
            body=self.html_content,
            from_email=settings.EMAIL_HOST_USER,
            to=self.recipient_list,
        )
        msg.content_subtype = "html"
        msg.send()


class EmailService:
    @staticmethod
    def send_email(
        email,
        verifcation_link,
    ) -> None:
        """_send email_

        Args:
            email (_type_): _message will send to this email_
            verifcation_link (_type_): _link to verify user's account_
        """

        html_template = "email_message.html"
        context = {
            "email": email,
            "verification_link": verifcation_link,
        }
        html_content = render_to_string(html_template, context)
        EmailThread(
            subject=f"Подверждение пользователя {email}",
            html_content=html_content,
            recipient_list=[email],
        ).start()
