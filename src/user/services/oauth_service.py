from typing import Union
from django.conf import settings
from django.http import HttpResponseRedirect

import requests
from urllib.parse import urlencode

from common.exceptions import *
from user.models import User


class GoogleOAuthService:
    user_model = User

    get_token_url = "https://www.googleapis.com/oauth2/v4/token"
    get_userinfo_url = "https://www.googleapis.com/oauth2/v2/userinfo"
    google_auth_url = "https://accounts.google.com/o/oauth2/v2/auth"

    @classmethod
    def google_login(cls, redirect_url: str) -> str:
        """_return url to google oauth_

        Args:
            redirect_url (str): _after successfully authorization of user, google will redirect user to this url_

        Returns:
            str: _return url to google oauth_
        """
        opts = {
            "scope": "email profile",
            "state": "security_token",
            "redirect_uri": redirect_url,
            "response_type": "code",
            "client_id": settings.GOOGLE_CLIENT_ID,
            "access_type": "offline",
        }
        url = f"{cls.google_auth_url}?{urlencode(opts)}"
        return url

    @classmethod
    def google_login_callback(
        cls,
        error: Union[str, None],
        code: Union[str, None],
        callback_url: str,
    ) -> Union[User, None]:
        """_google login callback handler_

        Args:
            error (Union[str, None]): _description_
            code (Union[str, None]): _description_
            callback_url (str): _description_

        Returns:
            Union[User, None]: _description_
        """
        if error:
            return HttpResponseRedirect(
                f"{settings.CLIENT_BASE_URL}/auth/google/login/callback/?status=error"
            )

        opts = {
            "code": code,
            "client_id": settings.GOOGLE_CLIENT_ID,
            "client_secret": settings.GOOGLE_SECRET,
            "redirect_uri": callback_url,
            "grant_type": "authorization_code",
        }
        token_info = requests.post(cls.get_token_url, data=opts)
        if token_info.status_code != 200:
            return HttpResponseRedirect(
                f"{settings.CLIENT_BASE_URL}/auth/google/login/callback/?status=error"
            )

        google_user = requests.get(
            cls.get_userinfo_url,
            headers={
                "Authorization": f"Bearer {token_info.json().get('access_token')}",
            },
        ).json()

        exist_user = cls.user_model.objects.filter(
            email=google_user.get("email"),
        )
        if exist_user.exists():
            user = exist_user.first()
            if not user.is_google_oauth:
                user.is_google_oauth = True
                user.is_active = True
                user.google_account_id = google_user.get("id")
            user.save()
            return user

        user = cls.user_model(
            google_account_id=google_user.get("id"),
            email=google_user.get("email"),
            full_name=google_user.get("name"),
            is_google_oauth=True,
            is_active=True,
            is_verified=google_user.get("verified_email"),
            default_language=google_user.get("locale").split("-")[0] or "ru",
        )
        user.save()
        return user
