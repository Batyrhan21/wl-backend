from django.urls import path, include

from user.api.oauth_views import (
    GoogleLoginAPIView,
    GoogleLoginCallbackAPIView,
)

from user.api.auth_views import (
    LogOutAPIView,
    LoginAPIView,
    ResendVerficationAPIView,
    SignUpAPIView,
    UserVerificationAPIView,
)
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
    path("login/", LoginAPIView.as_view(), name="login"),
    path("signup/", SignUpAPIView.as_view(), name="signup"),
    path(
        "google/login/callback/",
        GoogleLoginCallbackAPIView.as_view(),
        name="google-login-callback",
    ),
    path("google/login/", GoogleLoginAPIView.as_view(), name="google-login"),
    path("logout/", LogOutAPIView.as_view(), name="logout"),
    path("refresh/", jwt_views.TokenRefreshView.as_view(), name="token-refresh"),
    path("verify/resend/", ResendVerficationAPIView.as_view(), name="verify-resend"),
    path("verify/user/", UserVerificationAPIView.as_view(), name="verify-user"),
]
