from django.urls import path, include
from user.api.profile_views import (
    PasswordChangeAPIView,
    PasswordResetSendLinkAPIView,
    PasswordResetVerifyAPIView,
    ProfileAPIView,
)

urlpatterns = [
    path("", ProfileAPIView.as_view(), name="profile"),
    path("password/change/", PasswordChangeAPIView.as_view(), name="password-change"),
    path(
        "password/reset/send-link/",
        PasswordResetSendLinkAPIView.as_view(),
        name="password-reset-send-link",
    ),
    path(
        "password/reset/verify/",
        PasswordResetVerifyAPIView.as_view(),
        name="password-reset-verify",
    ),
]
